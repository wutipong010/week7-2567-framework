const express = require('express')
const app = express()
require('dotenv').config()

const port = process.env.PORT || 3000

// GET: http://localhost:3000/
app.get('/', (req, res) => {
    return res.status(200).json({ message: "GET request to the homepage!" })
})

// POST: http://localhost:3000/
app.post('/', (req, res) => {
    return res.status(201).json({ message: "POST request to the homepage!" })
})

// PUT: http://localhost:3000/
app.put('/', (req, res) => {
    return res.status(200).json({ message: "PUT request to the homepage!" })
})

// DELETE: http://localhost:3000/
app.delete('/', (req, res) => {
    return res.status(200).json({ message: "delete request to the homepage!" })
})

app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}`)
})